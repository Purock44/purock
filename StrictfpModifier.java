strictfp class StrictfpModifier{

	static double num1=0.1000;
	static double num2=2.444;

	//Strictfp uses the same precision everywhere
	public static void main(String[] args){

		StrictfpModifier sfm=new StrictfpModifier();
		System.out.println(sfm.sum(num1,num2));
	}

	public double sum(double num1,double num2)
	{
		return num1+num2;
	}

}
//you can use strictfp as
strictfp double sum(double num1,double num2){
	strictfp interface something{


	}
}