class  GrandFather{
	
	public String attitude(){
	return "nice";
	}
}

interface GrandMother{

	String voice();
}

interface Mother{

	String cooking();
}
public class Daughter extends GrandFather implements GrandMother,Mother{

	@Override
	public String voice(){

		return"polite";
	}

	@Override
	public String cooking(){
		return "tasty";
	}
	
	public static void main(String[] args){

		Daughter pu=new Daughter();
		System.out.println(pu.attitude());
		System.out.println(pu.voice());
		System.out.println(pu.cooking());

	}	


}