public class Calculator {

    public static void main(String[] args) {
        java.util.Scanner sc=new java.util.Scanner(System.in);

        System.out.print("Enter two numbers: ");

        // nextDouble() reads the next double from the keyboard
        double first = sc.nextDouble();
        double second = sc.nextDouble();

        System.out.print("Enter an operator (+, -, *, /): ");
        char operator = sc.next().charAt(0);

        double result;

        switch(operator)
        {
            case '+':
                result = first + second;
                break;

            case '-':
                result = first - second;
                break;

            case '*':
                result = first * second;
                break;

            case '/':
                result = first / second;
                break;

            // operator doesn't match any case constant (+, -, *, /)
            default:
                System.out.printf("Error! operator is not correct");
                return;
        }

        System.out.printf("The result is ="+result);
    }
}