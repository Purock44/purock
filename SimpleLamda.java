interface Hello{
	
	void hello();
}

public class SimpleLamda{
	
	public static void main(String[] args){

		//if the method  return type is void and has single expression
		//Hello h=()->System.out.println("hello i am from lamda experssion");
		

		// for the safe use methods brackets

		Hello h=()->{

			System.out.println("hello i am from lamda experssion");
		};
		h.hello();
	}
}