strictfp class MethodOverloading1{

	

	public double add(int a,double b){
		return a+b;
	}
	public double add(double a,int b){
		return a+b;
	}
	public double add(int a,double b){
		return a+b;
	}
	public int add(int a,int b){
		return a+b;
	}

	public float add(float a,float b,float c){
		return a+b+c;
	}

	public static void main(String[] args){

		MethodOverloading1 qw=new MethodOverloading1();
		System.out.println(qw.add(2.3,3.5));
		System.out.println(qw.add(2,3.5));
		System.out.println(qw.add(2,3));
		System.out.println(qw.add(2,3.5));

		System.out.println(qw.add(4,5f,3.4f));
		
	}
}