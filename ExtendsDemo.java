class ParentClass{

	public long add(long a,long b){

		return a+b;
	}
}
class SecondParent extends ParentClass{
	public long square(long a){
		return a*a;
	}
}

	public class ExtendsDemo extends SecondParent
	{
		@Override

		public long add (long a,long b){

			return a*b;
		}
      
		public static void main(String[] args){

			ExtendsDemo ed= new ExtendsDemo();
			System.out.println(ed.add(43,55));
			System.out.println(ed.square(67));
		}



	}
