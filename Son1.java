class Father
{

	public String getAttitude(){
		return "simple";
	}
}

public class Son1 extends Father{

	@Override
	public String getAttitude(){
		return "angry";
	}

	public static void main(String[] arss){

		Father s=new Father();
		System.out.println(s.getAttitude());

		Father sd=new Son1();
		System.out.println(sd.getAttitude());
	}
}