public class MethodOverloading{

	MethodOverloading(){

		System.out.println("hello i mam a blank constructor");
	}
	MethodOverloading(String name){
		System.out.println("hello"+name+"i am parametrized construtor");
	}

	public int add(int a,int b){
		return a+b;
	}
	public float add(float a,float b,float c){
		return a+b+c;
	}

	public static void main(String[] args){

		MethodOverloading mo=new MethodOverloading();
		System.out.println(mo.add(2,3));
		System.out.println(mo.add(4,5f,3.4f));
		System.out.println(MethodOverloading("hari"));

	}
}